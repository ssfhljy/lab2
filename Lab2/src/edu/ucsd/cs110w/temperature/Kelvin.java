/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wbe
 *
 */
public class Kelvin extends Temperature{

	public Kelvin(float v) {
		super(v);
	}

	public String toString()
	{
		return "" + this.getValue() + " K";
	}
	
	@Override
	public Temperature toCelsius() {
		Celsius c = new Celsius((this.getValue() - 273.0f));
		return c;
	}

	@Override
	public Temperature toFahrenheit() {
		Fahrenheit f = new Fahrenheit((this.getValue() - 273.0f) * 1.8f + 32.0f);
		return f;
	}

	@Override
	public Temperature toKelvin() {
		return this;
	}

}
