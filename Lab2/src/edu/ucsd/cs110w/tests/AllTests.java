package edu.ucsd.cs110w.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CelsiusTests.class, FahrenheitTests.class, KelvinTests.class })
public class AllTests {

}
